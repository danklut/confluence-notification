AJS.toInit(function ()
{

    console.log("Hello there");

    var baseUrl = AJS.contextPath();

    var renderPastNotifications = function ()
    {
        AJS.$.ajax({
            url:baseUrl + "/rest/notification/latest/",
            type:"GET",
            success:function (notifications)
            {
                var tbody = AJS.$("#notification-list tbody");
                tbody.empty();
                AJS.$("#notification-list-body").css("visibility", notifications.length > 0 ? "visible" : "hidden");

                for (var i = 0; i < notifications.length; i++)
                {
                    var notification = notifications[i];
                    tbody.append(renderNotification(notification));
                }
            },
            error:function (error)
            {
                console.log("An error happenned while rendering past notifications. " + error);
            }
        });
    }

    var renderNotification = function (notification)
    {
        return AJS.template.load("notification-template").fill({
            title:notification.item.title,
            message:notification.description,
            date:new Date(notification.created)
        })
    }

    var submitForm = function ()
    {
        console.log("Sending notification...");
        AJS.$.ajax({
            url:baseUrl + "/rest/notification/latest/",
            type:"POST",
            contentType:"application/json; charset=utf-8",
            data:JSON.stringify({
                title:[AJS.$("#notification-title").val()],
                message:[AJS.$("#notification-message").val()]
            }),
            success:function (data)
            {
                console.log("Ajax call was successful !")
                renderPastNotifications();
            },
            error:function (error)
            {
                console.log("Something went wrong...")
            }
        });
    }

    AJS.$("#submit").live('click', function (event)
    {
        event.stopPropagation();
        submitForm();
        return false;
    });

    renderPastNotifications();

});

