package com.example.plugins.tutorial.confluence.notification.servlet;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.auth.LoginUriProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(AdminServlet.class);

    private final PermissionManager permissionManager;
    private final TemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;

    public AdminServlet(PermissionManager permissionManager, TemplateRenderer renderer, LoginUriProvider loginUriProvider)
    {
        this.permissionManager = permissionManager ;
        this.renderer = renderer;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        if (!isAdmin())
        {
            redirectToLogin(req, resp);
            return;
        }

        resp.setContentType("text/html;charset=utf-8");
        renderer.render("templates/admin.vm", resp.getWriter());
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private boolean isAdmin()
    {
        return permissionManager.isConfluenceAdministrator(AuthenticatedUserThreadLocal.getUser());
    }

}